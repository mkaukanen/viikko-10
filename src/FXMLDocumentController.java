/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import netscape.javascript.JSException;

/**
 *
 * @author Miki
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private WebView web;
    @FXML
    private TextField urlField;
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button shoutOutButton;
    @FXML
    private Button reverseShoutOutButton;
    
    BrowseHistory BH = new BrowseHistory();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String initialSource = "www.google.fi";
        web.getEngine().load("https://"+initialSource);  
        urlField.setText(initialSource);
        BH.addToHistory(initialSource);
        }
    
    @FXML
    private void searchAction(javafx.scene.input.KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER){
            if (urlField.getText().equals("index.html")){
                web.getEngine().load(getClass().getResource("index.html").toExternalForm());
            }
            else{
                BH.addToHistory(urlField.getText());
                web.getEngine().load("https://" +urlField.getText());
            }
        }
    }         

    @FXML
    private void jumpBackAction(ActionEvent event) {
        String url;
        ArrayList<String> history = BH.getHistory();
        ListIterator litr = history.listIterator();
        int i=0;
        while (litr.hasNext()){
            i++;
            if((litr.next().toString()).equals(urlField.getText())){
                    break;
            }
        }
        try{
            String item = history.get(i-2);
            urlField.setText(item);
            web.getEngine().load("https://"+item);
        } catch(Exception e){
            System.out.println("(No previous page available)");
        }
    }
    

    @FXML
    private void jumpNextAction(ActionEvent event) {
        String url;
        ArrayList<String> history = BH.getHistory();
        ListIterator litr = history.listIterator();
        int i=0;
        while (litr.hasNext()){
            i++;
            if((litr.next().toString()).equals(urlField.getText())){
                break;
            }
        }
        try{
            String item = history.get(i);
            urlField.setText(item);
            web.getEngine().load("https://"+item);
        }catch(Exception e){
            System.out.println("(No next page available)");
        }
    } 

    @FXML
    private void refreshButtonAction(ActionEvent event) {
        web.getEngine().reload();
          
    }
    
    @FXML
    private void refreshAction(javafx.scene.input.KeyEvent event) {
        if (event.getCode()==KeyCode.F5){
            web.getEngine().reload();
        }
    }

    @FXML
    private void shoutOutAction(ActionEvent event) {
        try{
        web.getEngine().executeScript("document.shoutOut()");
        }
        catch(JSException e){
            System.err.println("TRY index.html at search first");
        }
    }

    @FXML
    private void reverseShoutOutAction(ActionEvent event) {
        try{
            web.getEngine().executeScript("initialize()");
        }
        catch(JSException e){
            System.err.println("TRY index.html at search first");
        }
    }

    @FXML
    private void checkPageAction(MouseEvent event) { //WORKS ONLY IF MOUSE PRESSED IN PAGE
        if (! web.getEngine().getLocation().toString().equals("https://"+urlField.getText())
                &&!(web.getEngine().getLocation().toString()).equals("https://www.google.fi/url?q=https")){
            String tokens[];
            tokens = web.getEngine().getLocation().toString().split("://");
            urlField.setText(tokens[1]);
            BH.addToHistory(tokens[1]);
        }
      
    }
 
}
