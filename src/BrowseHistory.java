
import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class BrowseHistory {
    ArrayList<String> history = new ArrayList<String>();
    
    
public void addToHistory(String url){
    if (history.size() <10){
        history.add(url);
    }
    else{
        history.remove(0);
        history.add(url);
    }
} 

public ArrayList<String> getHistory(){
    return history;
}
    
}
